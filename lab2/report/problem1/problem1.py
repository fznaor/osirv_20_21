import numpy as np
import cv2

img1 = cv2.imread('../../slike/airplane.bmp')
img2 = cv2.imread('../../slike/baboon.bmp')
img3 = cv2.imread('../../slike/pepper.bmp')

minHeight = np.min([img1.shape[0], img2.shape[0], img3.shape[0]])

img1 = img1[:minHeight,:,:]
img2 = img2[:minHeight,:,:]
img3 = img3[:minHeight,:,:]

rez = np.hstack((img1, img2, img3))

cv2.imwrite('spojene_slike.bmp', rez)
cv2.imshow('Spojene slike', rez)
cv2.waitKey(0)
cv2.destroyAllWindows()

def addFrame(img, frameWidth):
    height = img.shape[0]
    width = img.shape[1]
    img = np.hstack((np.zeros((height, frameWidth, 3), dtype = np.uint8), img, np.zeros((height, frameWidth, 3), dtype = np.uint8)))
    img = np.vstack((np.zeros((frameWidth, width + 2 * frameWidth, 3), dtype = np.uint8), img, np.zeros((frameWidth, width + 2 * frameWidth, 3), dtype = np.uint8)))
    return img
    
framedImg = cv2.imread('../../slike/pepper.bmp')
framedImg = addFrame(framedImg, 20)
cv2.imwrite('pepper_frame_20px.bmp', framedImg)