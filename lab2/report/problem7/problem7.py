import cv2
import numpy as np

img = cv2.imread('../../slike/baboon.bmp', 0)
img = cv2.resize(img, None, fx=0.25, fy=0.25, interpolation = cv2.INTER_CUBIC)
rows, cols = img.shape
imgs = []

for angle in range(0, 360, 30):
    M = cv2.getRotationMatrix2D((cols/2, rows/2), angle, 1)
    dst = cv2.warpAffine(img, M, (cols, rows))
    imgs.append(dst)

rez = np.hstack(imgs)
cv2.imwrite('baboon_rotated.bmp', rez)