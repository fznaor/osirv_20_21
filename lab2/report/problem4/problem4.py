import cv2
import os

imgs = os.listdir('../../slike')
thresholds = [63, 127, 191]

for imgName in imgs:
    img = cv2.imread('../../slike/' + imgName, 0)
    for threshold in thresholds:
        thr_img = img.copy()
        thr_img[thr_img < threshold] = 0
        cv2.imwrite(imgName.split('.')[0] + '_' + str(threshold) + '_thresh.png', thr_img)