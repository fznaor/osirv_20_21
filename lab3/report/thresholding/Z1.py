import cv2

boats = cv2.imread('../../slike/boats.bmp', 0)
baboon = cv2.imread('../../slike/baboon.bmp', 0)
airplane = cv2.imread('../../slike/airplane.bmp', 0)
images = [boats, baboon, airplane]
imgTitles = ['boats', 'baboon', 'airplane']
thresholds = [80, 127, 170]

for threshold in thresholds:
    for i in range(len(images)):
        ret,thresh1 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY)
        ret,thresh2 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY_INV)
        ret,thresh3 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TRUNC)
        ret,thresh4 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO)
        ret,thresh5 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO_INV)
        thresh6 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
        thresh7 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
      
        titles = ['BINARY','BINARY_INV','TRUNC','TOZERO','TOZERO_INV']
        newImages = [thresh1, thresh2, thresh3, thresh4, thresh5]
        
        for j in range(len(newImages)):
            cv2.imwrite(imgTitles[i] + '_' + titles[j] + '_thr_' + str(threshold) + '.jpg', newImages[j])

for i in range(len(images)):
    thresh6 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    thresh7 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    ret,thresh8 = cv2.threshold(images[i],0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    print(ret)
    
    titles = ['ADAPTIVE_MEAN', 'ADAPTIVE_GAUSSIAN', 'OTSU']
    newImages = [thresh6, thresh7, thresh8]
    
    for j in range(len(newImages)):
        cv2.imwrite(imgTitles[i] + '_' + titles[j] + '.jpg', newImages[j])