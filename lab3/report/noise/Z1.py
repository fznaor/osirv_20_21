import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def savehist(img, filename):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.savefig(filename)
  
img = cv2.imread('../../slike/boats.bmp', 0)

sigmas = [1, 5, 10, 20, 40, 60]
uniform = [20, 40, 60]
salt_n_pepper = [5, 10, 15, 20]

for sigma in sigmas:
    img_new = gaussian_noise(img, 0, sigma)
    cv2.imwrite('boats_gaussian_sigma_' + str(sigma) + '.jpg', img_new)
    savehist(img_new, 'hist_gaussian_sigma_' + str(sigma) + '.jpg')
    
for limit in uniform:
    img_new = uniform_noise(img, -1 * limit, limit)
    cv2.imwrite('boats_uniform_' + str(-1 * limit) + '_' + str(limit) + '.jpg', img_new)
    savehist(img_new, 'hist_uniform_' + str(-1 * limit) + '_' + str(limit) + '.jpg')
    
for percent in salt_n_pepper:
    img_new = salt_n_pepper_noise(img, percent)
    cv2.imwrite('boats_salt_and_pepper_' + str(percent) + '.jpg', img_new)
    savehist(img_new, 'hist_salt_and_pepper_' + str(percent) + '.jpg')