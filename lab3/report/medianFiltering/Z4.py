import numpy as np
import cv2

#funkcija za dodavanje dodatnog obruba na sliku kako prilikom korištenja filtera ne bi došlo do problema s granicama
def addBorders(arr, radius):
    newArr = np.zeros((arr.shape[0]+2*radius, arr.shape[1]+2*radius))
    newArr[radius:arr.shape[0]+radius, radius:arr.shape[1]+radius] = arr
    newArr[radius:arr.shape[0]+radius, 0:radius] = arr[:, 0:1]
    newArr[radius:arr.shape[0]+radius, arr.shape[1]+radius:arr.shape[1]+2*radius] = arr[:, arr.shape[1]-1:arr.shape[1]]
    newArr[0:radius, radius:arr.shape[1]+radius] = arr[0:1, :]
    newArr[arr.shape[0]+radius:arr.shape[0]+2*radius, radius:arr.shape[1]+radius] = arr[arr.shape[0]-1:arr.shape[0], :]
    newArr[0:radius, 0:radius] = arr[0, 0]
    newArr[0:radius, arr.shape[1]+radius:arr.shape[1]+2*radius] = arr[0, -1]
    newArr[arr.shape[0]+radius:arr.shape[0]+2*radius, 0:radius] = arr[-1, 0]
    newArr[arr.shape[0]+radius:arr.shape[0]+2*radius, arr.shape[1]+radius:arr.shape[1]+2*radius] = arr[-1, -1]
    return newArr

def myBlur(arr, radius):
    newArr = addBorders(arr, int(radius-1/2))
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            arr[i,j] = np.median(newArr[i:i+radius, j:j+radius])
    return arr

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

img = cv2.imread("../../slike/boats.bmp", 0)
img = salt_n_pepper_noise(img, 20)
cv2.imshow("Original",img)
cv2.imshow("OpenCV",cv2.medianBlur(img,3))
cv2.imshow("Custom verzija",myBlur(img,3)) #daje isti rezultat