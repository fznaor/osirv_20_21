import cv2
import numpy as np
import copy

img = cv2.imread('../../slike/chess.jpg')
img2 = copy.copy(img)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)

for blocksize in [1, 3, 5]:
    img2copy = copy.copy(img2)
    
    dst = cv2.cornerHarris(gray, blocksize, 3, 0.04)
    
    dst = cv2.dilate(dst, None)
    
    img2copy[dst > 0.01*dst.max()]=[0, 0, 255]
    cv2.imwrite('chess_harris_blocksize' + str(blocksize) + '.jpg', img2copy)