## Task 1

***Kod***

```python
import cv2

img = cv2.imread('../../slike/lenna.bmp', 0)
blurred = cv2.GaussianBlur(img, (5,5), 0)

lowerLimits = [0, 128, 255]
upperLimits = [255, 128, 255]

for i in range(3):
    cv2.imwrite('lenna_canny_' + str(lowerLimits[i]) + '_' + str(upperLimits[i]) + '.jpg', cv2.Canny(blurred, lowerLimits[i], upperLimits[i]))
```

***Rezultati***

lower = 0, upper = 255
![Canny_0_255](Task 1/lenna_canny_0_255.jpg)

lower = 128, upper = 128
![Canny_128_128](Task 1/lenna_canny_128_128.jpg)

lower = 255, upper = 255
![Canny_255_255](Task 1/lenna_canny_255_255.jpg)

Vrijednosti pragova 128 (i gornji i donji) daju najbolje rezultate za zadanu sliku. Povećavanje gornjeg praga na previsoku vrijednost rezultira gorom detekcijom rubova zbog veće strogoće u izboru potencijalnih rubova, dok povećavanje donjeg praga na previsoku vrijednost također pogoršava detekciju jer se velik broj točaka odmah odbacuje. S druge strane, stavljanje obaju pragova na iznimno male vrijednosti rezultirat će pretjerano osjetljivom detekcijom rubova koja neće dati nikakve korisne informacije o slici.




## Task 2

***Kod***

```python
import numpy as np
import cv2

def auto_canny(image, sigma=0.33):
        v = np.median(image)

        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)

        return edged
    
images = [cv2.imread("../../slike/airplane.bmp",0), cv2.imread("../../slike/barbara.bmp",0), cv2.imread("../../slike/boats.bmp",0), cv2.imread("../../slike/pepper.bmp",0)]
titles = ['airplane', 'barbara', 'boats', 'pepper']

for i in range(4):
    print(titles[i])
    auto = auto_canny(images[i])
    cv2.imwrite(titles[i] + '_canny.jpg', auto)
```

***Rezultati***

Slika `airplane.bmp` (lower = 134, upper = 255)
![Canny_a](Task 2/airplane_canny.jpg)

Slika `barbara.bmp` (lower = 72, upper = 143)
![Canny_ba](Task 2/barbara_canny.jpg)

Slika `boats.bmp` (lower = 92, upper = 183)
![Canny_bo](Task 2/boats_canny.jpg)

Slika `pepper.bmp` (lower = 81, upper = 160)
![Canny_p](Task 2/pepper_canny.jpg)




## Task 3

***Kod***

```python
import cv2
import numpy as np
import math
import copy

img = cv2.imread('../../slike/chess.jpg')
img2 = copy.copy(img)
gray= cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 166, 255)

for thta in [90, 180]:
    for threshold in [150, 200]:
        img2copy = copy.copy(img2)
        
        lines = cv2.HoughLines(edges, 1, math.pi/thta, threshold, np.array([]), 0, 0)
        
        a,b,c = lines.shape
        for i in range(a):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0, y0 = a*rho, b*rho
            pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
            pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
            cv2.line(img2copy, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)
        
        cv2.imwrite('chess_theta' + str(thta) + '_threshold' + str(threshold) + '.jpg', img2copy)
```

***Rezultati***

theta = 90, threshold = 150
![Hough_90_150](Task 3/chess_theta90_threshold150.jpg)

theta = 180, threshold = 200
![Hough_180_200](Task 3/chess_theta180_threshold200.jpg)

theta = 90, threshold = 200
![Hough_90_200](Task 3/chess_theta90_threshold200.jpg)

theta = 180, threshold = 150
![Hough_180_150](Task 3/chess_theta180_threshold150.jpg)

Najbolje rezultate daje kombinacija parametara theta = 90 i threshold = 150. Kombinacija theta = 180, threshold = 150 detektira neke dodatne linije, ali i više puta detektira iste linije dok vrijednost praga 200 rezultira smanjenjem broja detektiranih linija.




## Task 4

***Kod***

```python
import cv2
import numpy as np
import copy

img = cv2.imread('../../slike/chess.jpg')
img2 = copy.copy(img)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)

for blocksize in [1, 3, 5]:
    img2copy = copy.copy(img2)
    
    dst = cv2.cornerHarris(gray, blocksize, 3, 0.04)
    
    dst = cv2.dilate(dst, None)
    
    img2copy[dst > 0.01*dst.max()]=[0, 0, 255]
    cv2.imwrite('chess_harris_blocksize' + str(blocksize) + '.jpg', img2copy)
```

***Rezultati***

blocksize = 1
![Harris_1](Task 4/chess_harris_blocksize1.jpg)

blocksize = 3
![Harris_3](Task 4/chess_harris_blocksize3.jpg)

blocksize = 5
![Harris_5](Task 4/chess_harris_blocksize5.jpg)

Blocksize 1 ne detektira niti jedan kut na slici. Blocksize 3 i 5 detektiraju iste kutove na slici, razlika je u tome što blocksize 5 kao kut obuhvati više susjednih piksela od blocksizea 3.




## Task 5

***Kod***

```python
import cv2

img = cv2.imread('../../slike/roma_1.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

orb = cv2.ORB_create()
key_points, description = orb.detectAndCompute(img, None)

img_keypoints =cv2.drawKeypoints(img, key_points, img, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)


def image_detect_and_compute(detector, img_name):
    img = cv2.imread(img_name)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    kp, des = detector.detectAndCompute(img, None)
    return img, kp, des

def draw_image_matches(detector, img1_name, img2_name, nmatches=20):
    img1, kp1, des1 = image_detect_and_compute(detector, img1_name)
    img2, kp2, des2 = image_detect_and_compute(detector, img2_name)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key = lambda x: x.distance)

    img_matches = cv2.drawMatches(img1, kp1, img2, kp2, matches[:nmatches], img2, flags=2)

    cv2.imwrite('ORB_Interest_points.jpg', img_keypoints)
    cv2.imwrite('detector.jpg', img_matches)

orb = cv2.ORB_create()
draw_image_matches(orb, '../../slike/roma_1.jpg', '../../slike/roma_2.jpg')
```

***Rezultati***

ORB Interest points
![ORB](Task 5/ORB_Interest_points.jpg)

Detector
![Detector](Task 5/detector.jpg)