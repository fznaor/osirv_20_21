import cv2

img = cv2.imread('../../slike/lenna.bmp', 0)
blurred = cv2.GaussianBlur(img, (5,5), 0)

lowerLimits = [0, 128, 255]
upperLimits = [255, 128, 255]

for i in range(3):
    cv2.imwrite('lenna_canny_' + str(lowerLimits[i]) + '_' + str(upperLimits[i]) + '.jpg', cv2.Canny(blurred, lowerLimits[i], upperLimits[i]))