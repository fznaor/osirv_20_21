import numpy as np
import cv2

def auto_canny(image, sigma=0.33):
        v = np.median(image)

        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)

        return edged
    
images = [cv2.imread("../../slike/airplane.bmp",0), cv2.imread("../../slike/barbara.bmp",0), cv2.imread("../../slike/boats.bmp",0), cv2.imread("../../slike/pepper.bmp",0)]
titles = ['airplane', 'barbara', 'boats', 'pepper']

for i in range(4):
    print(titles[i])
    auto = auto_canny(images[i])
    cv2.imwrite(titles[i] + '_canny.jpg', auto)