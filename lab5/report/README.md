## Exercise 1

Pokretanjem skripte prikazuje se video `road.mp4`.

## Exercise 2

Pokretanjem skripte sprema se horizontalno zakrenut video `road.mp4`. Da bi se spremljeni video mogao reproducirati, bilo je potrebno spremiti ga u jednakim dimenzijama kao i originalni video (640, 480).

## Task 1

U kod iz Exercise 2 nije bilo potrebno promijeniti išta jer je linija `frame = cv2.flip(frame,1)` već radila horizontalno zakretanje videa. Ako se želi ponovno zaokrenuti video, moguće je obrisati spomenutu liniju ili dodati još jednu identičnu liniju.

## Exercise 3

Pokretanjem skripte na videu `road.mp4` vidljivi su rezultati Lucas-Kanade metode. Vidljive su linije pomicanja automobila i prometnih znakova u videu.

## Exercise 4

Pokretanjem skripte na videu `road.mp4` vidljivi su rezultati 'dense optical flow' metode. Za razliku od Lucas-Kanade metode, ovdje su vidljivi samo oni pikseli koji se kreću na slici, a njihova boja i intenzitet odgovora smjeru i brzini gibanja.

## Task 2

Metode za rijetki optički tok promatraju kretanje određenog podskupa piksela na videu (nama "zanimljivi" pikseli, npr. rubovi) dok metode za gusti optički tok promatraju kretanje svih piksela na videu što ih čini točnijima, ali i znatno sporijima.

## Exercise 5

Algoritam uspješno označava svaki automobil, no također daje veliki broj "false positive" rezultata.

## Exercise 6

Ova verzija implementacije daje puno bolje rezultate te označava isključivo automobile.

## Task 3

Za snimanje videa koristeći kameru s uređaja potrebno je funkciji `cv2.VideoCapture` predati parametar 0.