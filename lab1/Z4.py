import cv2

slika = cv2.imread('slike/pepper.bmp')

cv2.imshow('Vertikalno smanjena', slika[::2,:,:])
cv2.imshow('Horizontalno smanjena', slika[:,::2,:])
cv2.imshow('Smanjena u obje dimenzije', slika[::2,::2,:])

cv2.waitKey(0)
cv2.destroyAllWindows()