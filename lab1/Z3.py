import cv2
import numpy as np

slika = cv2.imread('slike/airplane.bmp')
height = slika.shape[0]
width = slika.shape[1]

slika = np.append(slika, np.zeros((height, 10, 3), dtype = np.uint8), axis = 1)
slika = np.append(np.zeros((height, 10, 3), dtype = np.uint8), slika, axis = 1)
slika = np.append(slika, np.zeros((10, width + 20, 3), dtype = np.uint8), axis = 0)
slika = np.append(np.zeros((10, width + 20, 3), dtype = np.uint8), slika, axis = 0)

#slika = cv2.copyMakeBorder(slika, 10, 10, 10, 10, cv2.BORDER_CONSTANT)

cv2.imwrite('slike/airplane_framed.bmp', slika)
