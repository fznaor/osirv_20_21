import cv2

slika = cv2.imread('slike/baboon.bmp')

plava = slika.copy()
plava[:,:,1:3] = 0
cv2.imshow('Plava', plava)

zelena = slika.copy()
zelena[:,:,::2] = 0
cv2.imshow('Zelena', zelena)

crvena = slika.copy()
crvena[:,:,0:2] = 0
cv2.imshow('Crvena', crvena)

cv2.waitKey(0)
cv2.destroyAllWindows()