import cv2

slika = cv2.imread('slike/baboon.bmp')

plava = slika.copy()
plava[:,:,1:3] = 0
cv2.imwrite('slike/plava.jpg', plava)

zelena = slika.copy()
zelena[:,:,::2] = 0
cv2.imwrite('slike/zelena.jpg', zelena)

crvena = slika.copy()
crvena[:,:,0:2] = 0
cv2.imwrite('slike/crvena.jpg', crvena)